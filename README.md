# Adv World Generator

This repository contains the source code for the AWG mod, which is
responsible for all the world and biomes generation process
used in the Adv game. Run `make` to compile the `awg` program
(Adv World and Biomes Generator).

## Usage

### Running AWG through the Adv Mods API

```json
{
  "class": "run",
  "path": "mods/awg/bin/awg",
  "input": inputObject
}
```

### Running AWG independently

```bash
/usr/share/adv/mods/awg/bin/awg [GAME_DIRECTORY] [INPUT_PATH]
```

If `GAME_DIRECTORY` is provided, AWG will change to that directory first.
If `INPUT_PATH` is not provided, AWG will read `./input.json`, as if it was
called through the Adv Mods API.

### The input object

The `input` object defines how will the world be generated, and accepts
the following parameters:

- `world_source` and `meta_source`: If boths are specified, a world is not
created from scratch. Instead, an already generated world is loaded from
these files and used as a starting point.
- `recipe`: This recipe shall be executed instead of the default recipe
(see below).
- `world_path`: Specifies the destination file where the generated world
shall be saved. Default is `./awg.json`.
- `log_path`: Log file path. If omitted, logs are disabled.
- `meta_path`: AWG provides a *Meta JSON* with additional information about
the generated world. This field specifies the path where this JSON will be
exported. Default is `./meta.json`.
- `world_class`: Specifies which class will the world object inherit.
Default is `world`.
- `ignore_mods`: String array that specifies mods to ignore when loading
biomes.
- `only_mods`: String array that specifies mods to read when loading biomes.
If omitted, every game mod is loaded. If both `only_mods` and `ignore_mods`
are given, then the biomes generator will only read the mods that are
on the `only_mods` list and are not on the `ignore_mods` list.
- `chrons`: Specifies how many chrons (big changes) will the world suffer
after its initialization. **Default is 0 chrons.**

**World attributes.** The input object also accepts all the additional
parameters that you can find at the [world reference file](assets/world.json),
together with all their corresponding default values.

In the world reference file you will find a `geometry` object.
There are two world geometries currently implemented: `planet` and `grid`.

The `planet` geometry only accepts a `size` field. The generated world
will be a `2*SIZE` by `SIZE` grid, with the borders connected in a way that
represents a hole planet.

The `grid` geometry accepts both `width` and `height` fields. The generated
world will be a `WIDTH` by `HEIGHT` grid, with the borders connected to
nothing. You may also specify only a `size` field, in which case the
generated world will be a `SIZE` by `SIZE` grid.
This geometry also accepts the optional parameter `central_latitude`.

To write attributes for the **final** world object, you must give them under
`attributes`.

Here's an example of input object for generating a world inside the `objects`
directory, logging at `awg.log` and ignoring biomes inside the `foo` mod:

```json
{
  "world_path": "objects/awg.json",
  "log_path": "awg.log",
  "ignore_mods": ["foo"],
  "chrons": 5,
  "geometry": {
    "type": "planet",
    "size": 64
  },
  "land_percentage": 40,
  "world_tilt": 13.0,
  "attributes": {
    "ticks_per_day": 8,
    "days_per_year": 360
  }
}
```

## Recipes

The world generation process (see below) has a standard *recipe*. This recipe
can be overwritten by giving a `recipe` string array through the input object.

This is the default recipe, which also shows all available tasks that can be
chosen in a custom recipe, any times and in any order:

```json
[
  "generateAltitudeAccidentPaths",
  "softenAltitudes",
  "initializeTemperature",
  "advanceChrons",
  "generateWaterfallPaths",
  "adjustSalinity",
  "generateBiomes",
  "generateCrusts",
  "generateEcoregions"
]
```

The `advanceChrons` task will advance the amount of chrons specified through
the input `chrons` field, every time this task is specified.
**Default is 0 chrons.**

## The Adv World

The world structure is based on *tiles* that contain:
- Neighbour tiles (north, north-east, east, south-east, south, south-west,
west and north-west);
- Coordinates (latitude and longitude);
- Some primary parameters: altitude, salinity, soil humidity and temperature;
and
- Some secondary parameters: Pressure, atmosphere humidity, humidity
absorption, effective temperature, soil quality, altitude accident boolean,
waterfall boolean, nwaterfalls integer and nerosions integer.

The design of the world generation process focuses on having a heuristic that
generates nice altitude maps. Having a realistic altitude map simplifies the
generation of all the remaining parameters.

### Tiles generation

First of all, AWG builds the desired world type (planet or grid) creating all
the tiles with their corresponding neighbours connected. Initially, every
parameter is constant through all tiles.

### Altitude accidents

Random paths are generated. Altitude deformations are applied to those tiles
and their orthogonal neighbours. The resulting altitude map is soften through
a low-pass filter.

### Temperature initialization

For every tile, an initial temperature value is given depending on the desired
temperature limits, each tile's latitude and the `world_tilt` input parameter.

### Advancing chrons

The following processes run `chrons` times:

- **Altitude shaping**:
Random tiles are grabbed and an erosion is applied: tile's altitude increases
while the altitudes of all neighbour tiles decrease.
Then, random tiles are grabbed and a collapse is applied: tiles's altitude
decreases while the altitudes of all neighbour tiles increase.
- **Temperature shaping**:
Random tiles are grabbed and their temperature is changed.

Then, the following adjustments are made:

- **Sea level adjustment**:
An offset is applied to every altitude value to give the desired land
percentage. This means that approximately `land_percentage`% of the tiles will
have an altitude that is larger than zero.
- **Altitude adjustment**:
Every altitude value is compressed to fulfill the desired altitude limits.
- **Soil humidity adjustment**:
For every tile that has a negative altitude, their soil humidity is increased
proportionally to the amount of neighbour tiles that also have negative
altitudes. This way *oceans* are modeled as tiles that have negative altitude
and high soil humidity.
- **Pressure adjustment**:
Pressures are adjusted, depending on the desired values and each tile's altitude.
Then presssure is slightly changed on randomly picked tiles.

### Waterfall paths generation

Random tiles are grabbed, and from there, random paths are generated, always
decreasing in altitude. On each path, the `nwaterfalls` value starts at 0,
and increases tile by tile. These values of `nwaterfalls` are assigned to these
corresponding tiles.
The value of `nwaterfalls` on tiles that don't belong to a waterfall path is -1.

### Salinity adjustment

Salinities are adjusted, depending on the desired values and each tile's altitude
and soil humidity.

### Biomes generation

Each tile in an Adv World can (and should) contain a **biome**.
Each biome is chosen among all available biomes in the game, which are
searched inside all `mods/<mod>/json/biomes/` directories, for every game mod.

For each tile parameter, a biome can specify conditions for that biome to
be generated on a certain tile. On each tile, after generating all the final
tile parameters, AWG reads all these conditions
to calculate the **probability of each biome to be generated on that tile**.
If conditions are met for a set of biomes to be generated, one of them shall
be chosen according to AWG's probability distribution.
The chosen biome is added to the tile's `classes` field.

### Crusts generation

If a tile doesn't happen to contain a biome after the biomes generation process,
a lower layer called **crust** is considered.
Each crust is chosen among all available crusts in the game, which are
searched inside all `mods/<mod>/json/crusts/` directories, for every game mod.
If a tile already contains a biome, no crust is added. Apart from this,
the crust selection process is the same as for biomes.

### Ecoregions

Biomes can belong to zero, one or many ecoregions.
Once biomes are generated, ecoregion objects are generated with their
attributes `area` and `perimeter`. Links of type `ecoregion` are built on all
their tiles, pointing to each respective ecoregion object.
The name of these links is `border` if the tile belongs to the ecoregion
perimeter and `interior` if it doesn't.
