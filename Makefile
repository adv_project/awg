SHELL := /bin/bash
ifndef PREFIX
  PREFIX = /usr
endif
SRCDIR := src
INCLUDEDIR := include
BUILDDIR := build
BINDIR := bin
MOD := awg
VERSION=$(shell git describe | rev | cut -d- -f2- | rev | sed -e 's/-/.r/' -e 's/^v//')
SRCEXT := cpp
VARS := -D PREFIX=\"'$(PREFIX)'\"
AWGSOURCES := $(shell echo $(SRCDIR)/{awg-main,Adv{World,Tile,BiomeManager}}.$(SRCEXT))
AWGOBJECTS := $(patsubst $(SRCDIR)/%,$(BUILDDIR)/%,$(AWGSOURCES:.$(SRCEXT)=.o))
CC := g++ -std=c++14 $(VARS)
INCLUDE := -I $(INCLUDEDIR)
LIB := -lm

default: awg

.PHONY: clean

.FORCE:

awg: $(AWGOBJECTS)
	@mkdir -p $(BINDIR)
	@echo -e "\e[1mLinking awg...\e[0m"
	@echo "CC $^ -o $(BINDIR)/awg $(LIB) $(LDFLAGS)"; $(CC) $^ -o $(BINDIR)/awg $(LIB) $(LDFLAGS)

$(BUILDDIR)/%.o: $(SRCDIR)/%.$(SRCEXT)
	@mkdir -p $(BUILDDIR)
	@echo "CC $(CFLAGS) $(INCLUDE) $(LDFLAGS) -c -o $@ $<"; $(CC) $(CFLAGS) $(INCLUDE) $(LDFLAGS) -c -o $@ $<

install:
	@mkdir -p $(DESTDIR)/$(PREFIX)/share/adv/mods/$(MOD)/bin
	test -x $(BINDIR)/awg && install -Dv -m 755 $(BINDIR)/awg $(DESTDIR)/$(PREFIX)/share/adv/mods/$(MOD)/bin/awg
	@mkdir -p $(DESTDIR)/$(PREFIX)/share/adv/mods/$(MOD)/assets
	install -v -m 644 assets/world.json $(DESTDIR)/$(PREFIX)/share/adv/mods/$(MOD)/assets/
	for jsondir in json json/crusts ; do \
		mkdir -p $(DESTDIR)/$(PREFIX)/share/adv/mods/$(MOD)/$$jsondir; \
		for jsonfile in $$jsondir/*.json ; do \
			install -Dv -m 644 $$jsonfile $(DESTDIR)/$(PREFIX)/share/adv/mods/$(MOD)/$$jsonfile ; \
		done; \
	done
	install -v -m 644 assets/properties.json $(DESTDIR)/$(PREFIX)/share/adv/mods/$(MOD)/
	@sed -i $(DESTDIR)/$(PREFIX)/share/adv/mods/$(MOD)/properties.json -e "s/[\"]version[\"]: [\"].*[\"]/\"version\": \"$(VERSION)\"/"

uninstall:
	@rm -r $(DESTDIR)/$(PREFIX)/share/adv/mods/$(MOD)

clean:
	@echo -e "\e[1mCleaning...\e[0m"; 
	@echo "$(RM) -r $(BUILDDIR) $(BINDIR)"; $(RM) -r $(BUILDDIR) $(BINDIR)
