/*
 * This file is part of the ADV Project
 * Text-based adventure game, highly modular and customizable
 * Written by Marcelo López Minnucci <coloconazo@gmail.com>
 * and Mariano López Minnucci <mlopezviedma@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ADVBIOMEMANAGER_H
#define ADVBIOMEMANAGER_H

#include <dirent.h>
#include "AdvWorld.h"

class AdvBiomeManager
{
  public:
    void importWorld(AdvWorld *world) { this->world = world; }
    void exportWorld() { world->exportWorld(); }
    void runRecipe();
    void loadBiomes(bool crust = false);
    void loadCrusts() { loadBiomes(true); }
    long generateBiomes(bool crust = false);
    long generateCrusts() { return generateBiomes(true); }
    void generateEcoregions();
    double getProbability(unsigned long biome, unsigned long tile);

    AdvWorld *world;
    std::vector<nlohmann::json> biomes;
    std::vector<nlohmann::json> crusts;
    nlohmann::json ecoregions;
};

#endif // ADVBIOMEMANAGER_H
