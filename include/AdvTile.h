/*
 * This file is part of the ADV Project
 * Text-based adventure game, highly modular and customizable
 * Written by Marcelo López Minnucci <coloconazo@gmail.com>
 * and Mariano López Minnucci <mlopezviedma@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ADVTILE_H
#define ADVTILE_H

#include <stdlib.h>
#include <time.h>
#include <string>
#include <fstream>
#include <vector>
#include <math.h>
#include "json.hpp"

class AdvTile;

struct neighbours_t {
  AdvTile *n, *ne, *e, *se, *s, *sw, *w, *nw;
};

class AdvTile
{
  public:
    AdvTile();
    void importJSON(std::string path);
    void refreshJSON();
    nlohmann::json exportJSON();
    void erosion(double strength, double weight = 1);
    void collapse(double strength, double weight = 1);
    void updateTemperature(double world_min, double world_max, double world_max_gradient);
    void updatePressure(double world_min, double world_max, double world_max_gradient);

    static bool hasParameter(nlohmann::json *j, std::string p)
      { return ( (*j).find(p) != (*j).end() ); }

    static const std::vector<std::string> PARAMETERS;

    nlohmann::json object; // Tile's imported/exported JSON
    double altitude, salinity, atm_humidity, soil_humidity, humidity_absorption,
           temperature, pressure, latitude, longitude; // Supported parameters
    neighbours_t neighbours; // Supported neighbours
    unsigned int nerosions;
    unsigned int nwaterfalls;
    bool is_altitude_accident;
    bool is_waterfall;
    std::string biome;
};

#endif // ADVTILE_H
