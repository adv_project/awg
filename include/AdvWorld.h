/*
 * This file is part of the ADV Project
 * Text-based adventure game, highly modular and customizable
 * Written by Marcelo López Minnucci <coloconazo@gmail.com>
 * and Mariano López Minnucci <mlopezviedma@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ADVWORLD_H
#define ADVWORLD_H

#include <string>
#include <unistd.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <fstream>
#include <iomanip>
#include <vector>
#include <chrono>
#include "AdvTile.h"

class AdvWorld
{
  public:
    AdvWorld();
    bool loadWorldFile();
    void refreshTiles();
    void exportWorld();
    bool generateWorld(nlohmann::json world);
    void advanceChron(unsigned int nchrons = 0);
    void log(std::string text, bool start = false);

    std::vector<AdvTile*> generatePath(unsigned int nsteps);
    void generateAltitudeAccidentPaths();
    void soften(unsigned int nsteps = 1);
    void initializeTemperature();
    void adjustSalinity();
    void adjustPressure();
    void adjustSeaLevel();
    void compressAltitude();
    void adjustSoilHumidity();
    void generateWaterfallPaths();
    double getLandPercentage(double sea_level);

    std::vector<AdvTile> tiles;
    nlohmann::json object; // World's imported/exported JSON
    nlohmann::json meta; // World's meta JSON containing relevant information

  private:
    void generatePlanet(unsigned long size);
    void generateGrid(unsigned long width, unsigned long height);
    void fixPercentage(nlohmann::json &param);
    void loadWorldObject(nlohmann::json world);

    std::vector<unsigned long> index; // Tiles permutation
    std::string world_file_path, world_class_name;
    std::chrono::high_resolution_clock::time_point log_reference_time;
    nlohmann::json ids_index; // World tiles indexed by id
};

#endif // ADVWORLD_H
