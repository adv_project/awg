/*
 * This file is part of the ADV Project
 * Text-based adventure game, highly modular and customizable
 * Written by Marcelo López Minnucci <coloconazo@gmail.com>
 * and Mariano López Minnucci <mlopezviedma@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "../include/AdvBiomeManager.h"

int main(int argc, char **argv)
{
  nlohmann::json input;
  AdvWorld world;
  AdvBiomeManager manager;
  std::ifstream input_file;
  std::string input_file_path;
  srand(time(0)); // Initialize srand()
  manager.importWorld(&world);
  if (argc > 1) chdir(argv[1]); // Change to the directory provided through argv[1]
  if (argc > 2)
    input_file_path = argv[2]; // Read input file from the path provided through argv[2]
  else
    input_file_path = "input.json"; // Read input file from "input.json"
  input_file.open(input_file_path);
  if (input_file.good()) {
    try { input_file >> input; } catch (...) {}
    input_file.close();
  }
  if (!world.generateWorld(input)) return 1;
  manager.runRecipe();
  manager.exportWorld();
  return 0;
}
