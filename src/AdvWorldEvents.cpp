/*
 * This file is part of the ADV Project
 * Text-based adventure game, highly modular and customizable
 * Written by Marcelo López Minnucci <coloconazo@gmail.com>
 * and Mariano López Minnucci <mlopezviedma@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

void AdvWorld::generateAltitudeAccidentPaths()
{
  log("Generating altitude accidents paths");
  std::vector<AdvTile*> path, n, m;
  double sqrt_size = sqrt(tiles.size());
  double sqrt_sqrt_size = sqrt(sqrt_size);
  double altitude_delta;
  double strength = (double) object["attributes"]["accidents"]["strength"];
  for (unsigned int i = 0; i < sqrt_sqrt_size; ++i) {
    altitude_delta = strength + strength*((double) rand()/(double) RAND_MAX);
    if (rand() % 2) altitude_delta = -altitude_delta;
    path = generatePath((0.75 + 1.25*((double) rand()/(double) RAND_MAX))*sqrt_size);
    for (auto& tile: path) {
      tile->is_altitude_accident = true;
      tile->altitude += altitude_delta;
      ++(tile->nerosions);
      n.clear();
      if (tile->neighbours.n) n.push_back(tile->neighbours.n);
      if (tile->neighbours.ne) n.push_back(tile->neighbours.ne);
      if (tile->neighbours.e) n.push_back(tile->neighbours.e);
      if (tile->neighbours.se) n.push_back(tile->neighbours.se);
      if (tile->neighbours.s) n.push_back(tile->neighbours.s);
      if (tile->neighbours.sw) n.push_back(tile->neighbours.sw);
      if (tile->neighbours.w) n.push_back(tile->neighbours.w);
      if (tile->neighbours.nw) n.push_back(tile->neighbours.nw);
      for (auto& x: n) {
        x->altitude += altitude_delta;
        m.clear();
        if (x->neighbours.n) m.push_back(x->neighbours.n);
        if (x->neighbours.ne) m.push_back(x->neighbours.ne);
        if (x->neighbours.e) m.push_back(x->neighbours.e);
        if (x->neighbours.se) m.push_back(x->neighbours.se);
        if (x->neighbours.s) m.push_back(x->neighbours.s);
        if (x->neighbours.sw) m.push_back(x->neighbours.sw);
        if (x->neighbours.w) m.push_back(x->neighbours.w);
        if (x->neighbours.nw) m.push_back(x->neighbours.nw);
        for (auto& y: m) {
          y->altitude += altitude_delta;
        }
      }
    }
  }
}

void AdvWorld::soften(unsigned int nsteps)
{
  log("Softening altitudes");
  std::vector<AdvTile*> n;
  double mean;
  for (unsigned int i = 0; i < nsteps; ++i) {
    for (auto& tile: tiles) {
      n.clear();
      if (tile.neighbours.n) n.push_back(tile.neighbours.n);
      if (tile.neighbours.ne) n.push_back(tile.neighbours.ne);
      if (tile.neighbours.e) n.push_back(tile.neighbours.e);
      if (tile.neighbours.se) n.push_back(tile.neighbours.se);
      if (tile.neighbours.s) n.push_back(tile.neighbours.s);
      if (tile.neighbours.sw) n.push_back(tile.neighbours.sw);
      if (tile.neighbours.w) n.push_back(tile.neighbours.w);
      if (tile.neighbours.nw) n.push_back(tile.neighbours.nw);
      mean = tile.altitude;
      for (auto& x: n) mean += x->altitude;
      mean /= (double) (n.size()+1);
      tile.altitude = mean;
      for (auto& x: n) {
        x->altitude = mean;
      }
    }
  }
}

void AdvWorld::initializeTemperature()
{
  log("Initializing temperatures");
  double min = (double) object["attributes"]["temperature"]["minimum"] +
    ((double) object["attributes"]["temperature"]["maximum"] - (double) object["attributes"]["temperature"]["minimum"])*
      0.05*((double) rand()/(double) RAND_MAX);
  double max = (double) object["attributes"]["temperature"]["maximum"] -
    ((double) object["attributes"]["temperature"]["maximum"] - (double) object["attributes"]["temperature"]["minimum"])*
      0.05*((double) rand()/(double) RAND_MAX);
  double gradient = (double) object["attributes"]["latitude_delta"]*(max - min)/90.0;
  if (gradient > (double) object["attributes"]["temperature"]["maximum_gradient"]/4.0) {
    gradient = (double) object["attributes"]["temperature"]["maximum_gradient"]/4.0;
    double mean = (max+min)/2.0;
    max = mean + 90.0*gradient/(double) object["attributes"]["latitude_delta"];
    min = mean - 90.0*gradient/(double) object["attributes"]["latitude_delta"];
  }
  double center = (min + max)/2.0;
  double amplitude = max - min;
  double z = abs((double) object["attributes"]["world_tilt"])/90.0;
  if (z > 1.0) z = 1.0;
  double sine_factor = amplitude/2.0;
  if (((double) object["attributes"]["world_tilt"]) < 0.0) sine_factor *= -1.0;
  for (auto& tile: tiles)
    tile.temperature = (1-z)*(min + amplitude*cos(3.1416/180.0 * (double) tile.latitude)) +
      z*(center + sine_factor*sin(3.1416/180.0 * (double) tile.latitude));
}

void AdvWorld::adjustSalinity()
{
  log("Adjusting salinity");
  for (auto& tile: tiles) {
    double soil_coeficient;
    if ( (tile.altitude < 10) && (tile.soil_humidity > 50.0) )
      soil_coeficient = tile.soil_humidity;
    else
      soil_coeficient = 100.0 - tile.soil_humidity;
    double r = soil_coeficient*(0.75 + 0.5*(double) rand()/(double) RAND_MAX);
    if (tile.temperature > 35.0) r *= (2.0 - exp(-(tile.temperature - 35.0)/10.0));
    if (r < (double) object["attributes"]["salinity"]["minimum"])
      r = (double) object["attributes"]["salinity"]["minimum"];
    if (r > (double) object["attributes"]["salinity"]["maximum"])
      r = object["attributes"]["salinity"]["maximum"];
    tile.salinity = r;
  }
}

void AdvWorld::adjustPressure()
{
  log("Adjusting pressure");
  double min = (double) object["attributes"]["pressure"]["minimum"] +
    ((double) object["attributes"]["pressure"]["default"] - (double) object["attributes"]["pressure"]["minimum"])*
      0.05*((double) rand()/(double) RAND_MAX);
  double max = (double) object["attributes"]["pressure"]["default"] -
    ((double) object["attributes"]["pressure"]["default"] - (double) object["attributes"]["pressure"]["minimum"])*
      0.05*((double) rand()/(double) RAND_MAX);
  double gradient = (double) (max - min)/6000.0;
  if (gradient > (double) object["attributes"]["pressure"]["maximum_gradient"]/4.0) {
    gradient = (double) object["attributes"]["pressure"]["maximum_gradient"]/4.0;
    double mean = (max+min)/2.0;
    max = mean + 6000.0*gradient;
    min = mean - 6000.0*gradient;
  }
  for (auto& tile: tiles) {
    double effective_altitude;
    if ( (tile.altitude < 0) && (tile.soil_humidity > 50.0) )
      effective_altitude = 0;
    else
      effective_altitude = tile.altitude;
    tile.pressure = max - effective_altitude*(max-min)/6000.0;
  }
}

void AdvWorld::adjustSeaLevel()
{
  log("Adjusting sea level");
  double delta;
  double step = 1000;
  double land_percentage = getLandPercentage(0);
  bool smaller;
  if (land_percentage < (double) object["attributes"]["land_percentage"]) {
    delta = -step;
    smaller = true;
  }
  else {
    delta = step;
    smaller = false;
  }
  while ( (abs(land_percentage - (double) object["attributes"]["land_percentage"]) > 1) && (step > 1) ) {
    land_percentage = getLandPercentage(delta);
    if (land_percentage < object["attributes"]["land_percentage"]) {
      if (!smaller) step /= 2;
      delta -= step;
      smaller = true;
    }
    else {
      if (smaller) step /= 2;
      delta += step;
      smaller = false;
    }
  }
  for (auto& tile: tiles)
    tile.altitude -= delta;
}

void AdvWorld::compressAltitude()
{
  log("Compressing altitudes");
  double min_altitude = (double) object["attributes"]["altitude"]["minimum"];
  double max_altitude = (double) object["attributes"]["altitude"]["maximum"];
  double rand_minimum = min_altitude + (0.025 * (double) rand()/(double) RAND_MAX)*(max_altitude - min_altitude);
  double rand_maximum = max_altitude - (0.025 * (double) rand()/(double) RAND_MAX)*(max_altitude - min_altitude);
  min_altitude = max_altitude = tiles[0].altitude;
  for (auto& tile: tiles) {
    if (tile.altitude < min_altitude) min_altitude = tile.altitude;
    if (tile.altitude > max_altitude) max_altitude = tile.altitude;
  }
  for (auto& tile: tiles) {
    if ((tile.altitude < 0) && (min_altitude < rand_minimum))
      tile.altitude = -abs(rand_minimum*tile.altitude/min_altitude);
    else if ((tile.altitude > 0) && (max_altitude > rand_maximum))
      tile.altitude = rand_maximum*tile.altitude/max_altitude;
  }
}

void AdvWorld::adjustSoilHumidity()
{
  log("Adjusting soil humidity");
  std::vector<AdvTile*> n;
  unsigned int nseas;
  double mean;
  for (auto& tile: tiles) {
    double r = (double) rand()/(double) RAND_MAX;
    if ( (tile.temperature > 35.0) && (rand() % 2) ) r *= exp(-(tile.temperature - 35.0)/5.0);
    tile.soil_humidity = (double) object["attributes"]["soil_humidity"]["minimum"] + ((double)
      object["attributes"]["soil_humidity"]["maximum"] - (double) object["attributes"]["soil_humidity"]["minimum"])*r;
  }
  for (auto& tile: tiles) {
    n.clear();
    if (tile.neighbours.n) n.push_back(tile.neighbours.n);
    if (tile.neighbours.ne) n.push_back(tile.neighbours.ne);
    if (tile.neighbours.e) n.push_back(tile.neighbours.e);
    if (tile.neighbours.se) n.push_back(tile.neighbours.se);
    if (tile.neighbours.s) n.push_back(tile.neighbours.s);
    if (tile.neighbours.sw) n.push_back(tile.neighbours.sw);
    if (tile.neighbours.w) n.push_back(tile.neighbours.w);
    if (tile.neighbours.nw) n.push_back(tile.neighbours.nw);
    mean = tile.soil_humidity;
    for (auto& x: n) mean += x->soil_humidity;
    mean /= (double) (n.size()+1);
    tile.soil_humidity = mean*(0.75 + 0.5*((double) rand()/(double) RAND_MAX));
  }
  for (auto& tile: tiles) {
    n.clear();
    if (tile.neighbours.n) n.push_back(tile.neighbours.n);
    if (tile.neighbours.ne) n.push_back(tile.neighbours.ne);
    if (tile.neighbours.e) n.push_back(tile.neighbours.e);
    if (tile.neighbours.se) n.push_back(tile.neighbours.se);
    if (tile.neighbours.s) n.push_back(tile.neighbours.s);
    if (tile.neighbours.sw) n.push_back(tile.neighbours.sw);
    if (tile.neighbours.w) n.push_back(tile.neighbours.w);
    if (tile.neighbours.nw) n.push_back(tile.neighbours.nw);
    if (!n.size()) continue;
    nseas = 0;
    for (auto& x: n) if (x->altitude < 0) ++nseas;
    if ( (tile.altitude < 0) && (nseas >= n.size()/2) ) {
      tile.soil_humidity = 100.0*(((double) nseas)/((double) n.size()) + 0.2*((double) rand()/(double) RAND_MAX));
      if (tile.soil_humidity > 100.0) tile.soil_humidity = 100;
    }
  }
}

void AdvWorld::generateWaterfallPaths()
{
  log("Generating waterfalls");
  std::vector<AdvTile*> n;
  AdvTile *t, *u;
  double wp = ((double) object["attributes"]["waterfall"]["percentage"])/100.0;
  double wr = ((double) object["attributes"]["waterfall"]["randomness"])/100.0;
  unsigned int amount, nwaterfalls;
  double min_altitude;
  std::random_shuffle(index.begin(), index.end());
  amount = (wp-wr + 2.0*wr*((double) rand()/(double) RAND_MAX))*tiles.size();
  if (amount > tiles.size()) amount = tiles.size();
  for (int i = 0; i < amount; ++i) {
    nwaterfalls = 0;
    t = &tiles.at(index.at(i));
    while (t) {
      t->is_waterfall = true;
      t->nwaterfalls = nwaterfalls++;
      n.clear();
      if (t->neighbours.n) n.push_back(t->neighbours.n);
      if (t->neighbours.e) n.push_back(t->neighbours.e);
      if (t->neighbours.s) n.push_back(t->neighbours.s);
      if (t->neighbours.w) n.push_back(t->neighbours.w);
      if (!n.size()) break;
      u = t;
      for (auto& x: n) if (x->altitude < u->altitude) u = x;
      if (u == t) break;
      t = u;
    }
  }
}
