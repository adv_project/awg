/*
 * This file is part of the ADV Project
 * Text-based adventure game, highly modular and customizable
 * Written by Marcelo López Minnucci <coloconazo@gmail.com>
 * and Mariano López Minnucci <mlopezviedma@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "../include/AdvBiomeManager.h"

void AdvBiomeManager::runRecipe()
{
  bool tiles_refreshed = false;
  loadCrusts();
  loadBiomes();
  for (int i = 0; i < world->object["recipe"].size(); ++i) {
    if (world->object["recipe"][i] == "generateAltitudeAccidentPaths") {
      world->generateAltitudeAccidentPaths();
      tiles_refreshed = false;
    }
    else if (world->object["recipe"][i] == "softenAltitudes") {
      world->soften();
      tiles_refreshed = false;
    }
    else if (world->object["recipe"][i] == "initializeTemperature") {
      world->initializeTemperature();
      tiles_refreshed = false;
    }
    else if (world->object["recipe"][i] == "softenAltitudes") {
      world->soften();
      tiles_refreshed = false;
    }
    else if (world->object["recipe"][i] == "advanceChrons") {
      world->advanceChron(world->object["chrons"]);
      tiles_refreshed = false;
    }
    else if (world->object["recipe"][i] == "generateWaterfallPaths") {
      world->generateWaterfallPaths();
      tiles_refreshed = false;
    }
    else if (world->object["recipe"][i] == "adjustSalinity") {
      world->adjustSalinity();
      tiles_refreshed = false;
    }
    else if (world->object["recipe"][i] == "generateBiomes") {
      if (!tiles_refreshed) {
        world->refreshTiles();
        tiles_refreshed = true;
      }
      generateBiomes();
    }
    else if (world->object["recipe"][i] == "generateCrusts") {
      if (!tiles_refreshed) {
        world->refreshTiles();
        tiles_refreshed = true;
      }
      generateCrusts();
    }
    else if (world->object["recipe"][i] == "generateEcoregions") {
      generateEcoregions();
    }
  }
  if (!tiles_refreshed) world->refreshTiles();
}

void AdvBiomeManager::loadBiomes(bool crust)
{
  std::string directory, type;
  if (crust) {
    directory = "crusts";
    type = "crust";
  }
  else {
    directory = "biomes";
    type = "biome";
  }
  world->log(std::string("Loading ") + directory + " from mods");
  std::vector<nlohmann::json> array;
  world->meta[directory] = nlohmann::json::parse("{}");
  DIR *moddir, *biomesdir;
  struct dirent *modentry, *biomeentry;
  std::vector<std::string> mods = world->object["only_mods"].get<std::vector<std::string>>();
  std::string mods_list;
  std::ifstream biome_file;
  nlohmann::json biome;
  char *dirname;
  if (mods.empty() && ((dirname = realpath("mods", NULL)) != NULL)) {
    if ((moddir = opendir(dirname)) != NULL) {
      while ((modentry = readdir(moddir)) != NULL) {
        if ( !strcmp(modentry->d_name, ".") || !strcmp(modentry->d_name, "..") ) continue;
        mods.push_back(modentry->d_name);
      }
      closedir(moddir);
    }
    free(dirname);
  }
  for (int i = 0; i < world->object["ignore_mods"].size(); ++i)
    for (int j = 0; j < mods.size(); ++j)
      if (mods[j] == world->object["ignore_mods"][i]) {
        mods.erase(mods.begin() + j);
        break;
      }
  for (auto& mod: mods) {
    mods_list += " " + mod;
    dirname = realpath(std::string(std::string("mods/") + mod + "/json/" + directory).c_str(), NULL);
    if (dirname != NULL) {
      if ((biomesdir = opendir(dirname)) != NULL) {
        while ((biomeentry = readdir(biomesdir)) != NULL) {
          if ( !strcmp(biomeentry->d_name, ".") || !strcmp(biomeentry->d_name, "..") ) continue;
          biome.clear();
          biome_file.open(std::string(std::string("mods/") + mod + "/json/"+ directory + "/" + biomeentry->d_name));
          if (biome_file.good()) {
            try { biome_file >> biome; } catch (...) {}
            biome_file.close();
          }
          if (!AdvTile::hasParameter(&biome, "id") || biome["id"].type() != nlohmann::json::value_t::string ||
            biome["id"].get<std::string>().empty()) continue;
          world->meta[directory][biome["id"].get<std::string>()] = 0;
          if (!AdvTile::hasParameter(&biome, "init") || biome["init"].type() != nlohmann::json::value_t::array) continue;
          for (int c = 0; c < biome["init"].size(); ++c)
            if (!AdvTile::hasParameter(&biome["init"][c], "attributes") ||
              biome["init"][c]["attributes"].type() != nlohmann::json::value_t::object) {
              biome["init"].erase(c);
              --c;
            }
          if (!biome["init"].size()) continue;
          if (AdvTile::hasParameter(&biome, "type") && biome["type"].type() == nlohmann::json::value_t::string &&
            biome["type"].get<std::string>() == type) array.push_back(biome);
          if (AdvTile::hasParameter(&biome, "ecoregions") && biome["ecoregions"].type() == nlohmann::json::value_t::array &&
            biome["ecoregions"].size() > 0 && biome["ecoregions"][0].type() == nlohmann::json::value_t::string)
            for (int i = 0; i < biome["ecoregions"].size(); ++i) {
              ecoregions[directory][biome["id"].get<std::string>()][biome["ecoregions"][i].get<std::string>()] = true;
              ecoregions["ecoregions"][biome["ecoregions"][i].get<std::string>()] = true;
            }
        }
        closedir(biomesdir);
      }
      free(dirname);
    }
  }
  world->log(std::to_string(array.size()) + " " + directory + " found on these mods:" + mods_list);
  if (crust) crusts = array; else biomes = array;
}

long AdvBiomeManager::generateBiomes(bool crust)
{
  std::string directory;
  std::vector<nlohmann::json> biomes_backup;
  if (crust) directory = "crusts"; else directory = "biomes";
  world->log(std::string("Generating ") + directory + " for all tiles");
  double r, candidate;
  long nbiomes = 0;
  std::vector<double> probabilities;
  if (crust) {
    if (!crusts.size()) return 0;
    biomes_backup = biomes;
    biomes = crusts;
  }
  else {
    if (!biomes.size()) return 0;
  }
  probabilities.resize(biomes.size(), 0);
  for (int t = 0; t < world->tiles.size(); ++t) {
    if (!crust) world->tiles[t].biome.clear();
    if (crust && !world->tiles[t].biome.empty()) continue;
    // Calculate all biomes probability
    for (int b = 0; b < biomes.size(); ++b) {
      probabilities[b] = getProbability(b, t);
      if (b > 0) probabilities[b] += probabilities[b-1];
    }
    if (probabilities[probabilities.size()-1] <= std::numeric_limits<double>::epsilon()) continue;
    for (int p = 0; p < probabilities.size(); ++p) probabilities[p] /= probabilities[probabilities.size()-1];
    r = (double) rand()/(double) RAND_MAX;
    for (candidate = 0; candidate < probabilities.size(); ++candidate) if (r <= probabilities[candidate]) break;
    if (candidate < probabilities.size()) { // Put candidate inside tile
      world->tiles[t].biome = biomes[candidate]["id"].get<std::string>();
      world->meta[directory][biomes[candidate]["id"].get<std::string>()] =
      (unsigned long) world->meta[directory][biomes[candidate]["id"].get<std::string>()] + 1;
      ++nbiomes;
    }
  }
  if (crust) biomes = biomes_backup;
  return nbiomes;
}

void AdvBiomeManager::generateEcoregions()
{
  world->log("Generating world ecoregions");
  std::string id, local_class, neighbour_class;
  std::vector<AdvTile*> neighbours;
  unsigned long n_eq_classes;
  bool is_border, found;
  unsigned long idx, merge_idx, area, perimeter;
  std::vector<std::string> classes_index;
  std::vector<nlohmann::json> classes_jsons;
  ecoregions["classes"] = nlohmann::json::parse("{}");
  for (auto& x: ecoregions["ecoregions"].items()) { // Process every ecoregion
    world->log(std::string("Building instances of the ") + x.key() + " ecoregion");
    ecoregions["classes"][x.key()]["tiles"] = nlohmann::json::parse("{}");
    ecoregions["classes"][x.key()]["classes"] = nlohmann::json::parse("{}");
    n_eq_classes = 1;
    for (int t = 0; t < world->tiles.size(); ++t) { // Process every tile for this ecoregion
      // If this tile doesn't inherit a biome, skip it
      if (world->tiles[t].biome.empty()) continue;
      // If this tile doesn't belong to this ecoregion, skip it
      if (!AdvTile::hasParameter(&ecoregions["biomes"][world->tiles[t].biome], x.key())) continue;
      id = world->tiles[t].object["id"];
      neighbours.clear();
      if (world->tiles[t].neighbours.n) neighbours.push_back(world->tiles[t].neighbours.n);
      if (world->tiles[t].neighbours.nw) neighbours.push_back(world->tiles[t].neighbours.nw);
      if (world->tiles[t].neighbours.w) neighbours.push_back(world->tiles[t].neighbours.w);
      if (world->tiles[t].neighbours.sw) neighbours.push_back(world->tiles[t].neighbours.sw);
      if (world->tiles[t].neighbours.s) neighbours.push_back(world->tiles[t].neighbours.s);
      if (world->tiles[t].neighbours.se) neighbours.push_back(world->tiles[t].neighbours.se);
      if (world->tiles[t].neighbours.e) neighbours.push_back(world->tiles[t].neighbours.e);
      if (world->tiles[t].neighbours.ne) neighbours.push_back(world->tiles[t].neighbours.ne);
      is_border = false;
      // Hasn't this tile been already visited?
      if (!AdvTile::hasParameter(&ecoregions["classes"][x.key()]["tiles"], id)) {
        // Pick a class name for this ecoregion
        ecoregions["classes"][x.key()]["tiles"][id]["class"] = x.key() + std::to_string(n_eq_classes++);
        local_class = ecoregions["classes"][x.key()]["tiles"][id]["class"];
        ecoregions["classes"][x.key()]["classes"][local_class]["tiles"][id] = true;
        ecoregions["classes"][x.key()]["classes"][local_class]["classes"][local_class] = true;
      }
      else {
        local_class = ecoregions["classes"][x.key()]["tiles"][id]["class"];
      }
      for (auto& y: neighbours) {
        // Doesn't this neighbour belong to this ecoregion?
        if (!y->biome.empty() && !AdvTile::hasParameter(&ecoregions["biomes"][y->biome], x.key())) {
          is_border = true;
          continue;
        }
        // Hasn't this neighbour been already visited?
        if (!AdvTile::hasParameter(&ecoregions["classes"][x.key()]["tiles"], y->object["id"])) {
          ecoregions["classes"][x.key()]["tiles"][y->object["id"].get<std::string>()]["class"] = local_class;
          ecoregions["classes"][x.key()]["tiles"][y->object["id"].get<std::string>()]["border"] = false;
          ecoregions["classes"][x.key()]["classes"][local_class]["tiles"][y->object["id"].get<std::string>()] = true;
        }
        else {
          neighbour_class = ecoregions["classes"][x.key()]["tiles"][y->object["id"].get<std::string>()]["class"];
          if (local_class != neighbour_class) {
            ecoregions["classes"][x.key()]["classes"][neighbour_class]["classes"][local_class] = true;
            ecoregions["classes"][x.key()]["classes"][local_class]["classes"][neighbour_class] = true;
          }
        }
      }
      ecoregions["classes"][x.key()]["tiles"][id]["border"] = is_border;
    }
    // Merge all equivalent classes for this ecoregion
    classes_index.clear();
    classes_jsons.clear();
    for (auto& c: ecoregions["classes"][x.key()]["classes"].items()) {
      classes_index.push_back(c.key());
      classes_jsons.push_back(c.value());
    }
    found = true;
    while (found) {
      idx = 0;
      found = false;
      while (idx < classes_index.size()) {
        merge_idx = idx + 1;
        while (merge_idx < classes_index.size()) {
          if (AdvTile::hasParameter(&classes_jsons[merge_idx]["classes"], classes_index[idx]) ||
            AdvTile::hasParameter(&classes_jsons[idx]["classes"], classes_index[merge_idx])) {
            classes_jsons[idx].merge_patch(classes_jsons[merge_idx]);
            classes_index.erase(classes_index.begin() + merge_idx);
            classes_jsons.erase(classes_jsons.begin() + merge_idx);
          found = true;
          }
          else {
            ++merge_idx;
          }
        }
        ++idx;
      }
    }
    ecoregions["classes"][x.key()]["classes"].clear();
    for (unsigned long i = 0; i < classes_index.size(); ++i) { // Map equivalent ecoregions correctly
      ecoregions["classes"][x.key()]["classes"][classes_index[i]] = classes_jsons[i];
      area = 0;
      perimeter = 0;
      for (auto& y: ecoregions["classes"][x.key()]["classes"][classes_index[i]]["tiles"].items()) {
        ecoregions["classes"][x.key()]["tiles"][y.key()]["class"] = classes_index[i];
        ++area;
        if (ecoregions["classes"][x.key()]["tiles"][y.key()]["border"]) ++perimeter;
      }
      ecoregions["classes"][x.key()]["classes"][classes_index[i]].clear();
      ecoregions["classes"][x.key()]["classes"][classes_index[i]]["area"] = area;
      ecoregions["classes"][x.key()]["classes"][classes_index[i]]["perimeter"] = perimeter;
    }
  }
  world->meta["ecoregions"] = ecoregions["classes"];
}

// World tiles should be refreshed to avoid wrong values or exceptions thrown because of undefined JSON attributes
double AdvBiomeManager::getProbability(unsigned long biome, unsigned long tile)
{
  double distance, distance_to_minimum, distance_to_maximum, condition_probability, probability;
  probability = 0;
  for (int c = 0; c < biomes[biome]["init"].size(); ++c) {
    condition_probability = 1;
    for (int p = 0; p < AdvTile::PARAMETERS.size(); ++p) {
      if (!AdvTile::hasParameter(&biomes[biome]["init"][c]["attributes"], AdvTile::PARAMETERS[p])) continue;
      distance_to_minimum = distance_to_maximum = -1;
      if (AdvTile::hasParameter(&biomes[biome]["init"][c]["attributes"][AdvTile::PARAMETERS[p]], "minimum")) {
        if ((double) world->tiles[tile].object["attributes"][AdvTile::PARAMETERS[p]] <=
          (double) biomes[biome]["init"][c]["attributes"][AdvTile::PARAMETERS[p]]["minimum"]) {
          condition_probability = 0;
          break;
        }
        else distance_to_minimum = (double) world->tiles[tile].object["attributes"][AdvTile::PARAMETERS[p]] -
          (double) biomes[biome]["init"][c]["attributes"][AdvTile::PARAMETERS[p]]["minimum"];
      }
      if (AdvTile::hasParameter(&biomes[biome]["init"][c]["attributes"][AdvTile::PARAMETERS[p]], "maximum")) {
        if ((double) world->tiles[tile].object["attributes"][AdvTile::PARAMETERS[p]] >=
          (double) biomes[biome]["init"][c]["attributes"][AdvTile::PARAMETERS[p]]["maximum"]) {
          condition_probability = 0;
          break;
        }
        else distance_to_maximum = (double) biomes[biome]["init"][c]["attributes"][AdvTile::PARAMETERS[p]]["maximum"] -
          (double) world->tiles[tile].object["attributes"][AdvTile::PARAMETERS[p]];
      }
      if ( (distance_to_minimum < 0) || (distance_to_maximum < 0) ) continue;
      (distance_to_minimum < distance_to_maximum) ? distance = distance_to_minimum : distance = distance_to_maximum;
      condition_probability *= (2.0*distance/(distance_to_minimum + distance_to_maximum));
      if (condition_probability <= std::numeric_limits<double>::epsilon()) break;
    }
    if (condition_probability > probability) probability = condition_probability;
  }
  return probability;
}
