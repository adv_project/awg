/*
 * This file is part of the ADV Project
 * Text-based adventure game, highly modular and customizable
 * Written by Marcelo López Minnucci <coloconazo@gmail.com>
 * and Mariano López Minnucci <mlopezviedma@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "../include/AdvTile.h"
#include "AdvTileEvents.cpp"

const std::vector<std::string> AdvTile::PARAMETERS = {"latitude", "longitude", "altitude", "salinity",
  "atm_humidity", "soil_humidity", "humidity_absorption", "temperature", "pressure",
  "effective_temperature", "soil_quality", "nwaterfalls" };

AdvTile::AdvTile()
{
  if (!AdvTile::hasParameter(&object, "classes")) object["classes"] = nlohmann::json::parse("[]");
  neighbours.n = neighbours.ne = neighbours.e = neighbours.se = neighbours.s = neighbours.sw = neighbours.w = neighbours.nw = nullptr;
  nerosions = 0;
  nwaterfalls = -1;
  is_altitude_accident = false;
  is_waterfall = false;
}

// Refresh JSON attributes
void AdvTile::refreshJSON()
{
  object["attributes"]["altitude"] = (int)(altitude*10.0)/10.0;
  object["attributes"]["salinity"] = (int)(salinity*10.0)/10.0;
  object["attributes"]["atm_humidity"] = (int)(atm_humidity*10.0)/10.0;
  object["attributes"]["soil_humidity"] = (int)(soil_humidity*10.0)/10.0;
  object["attributes"]["humidity_absorption"] = (int)(humidity_absorption*10.0)/10.0;
  object["attributes"]["temperature"] = (int)(temperature*10.0)/10.0;
  if (altitude <= 0.0)
    object["attributes"]["effective_temperature"] = (int)(temperature*10.0)/10.0;
  else
    object["attributes"]["effective_temperature"] = (int)((temperature-altitude/350.0)*10.0)/10.0;
  object["attributes"]["soil_quality"] = (int)((100+soil_humidity-salinity)*5.0)/10.0;
  object["attributes"]["pressure"] = (int)(pressure*100.0)/100.0;
  object["attributes"]["latitude"] = (int)(latitude*1000000.0)/1000000.0;
  object["attributes"]["longitude"] = (int)(longitude*1000000.0)/1000000.0;
  object["attributes"]["is_altitude_accident"] = (bool) is_altitude_accident;
  object["attributes"]["is_waterfall"] = (bool) is_waterfall;
  object["attributes"]["nwaterfalls"] = (int) nwaterfalls;
  object["attributes"]["nerosions"] = (int) nerosions;
}

// Write tile in JSON file
nlohmann::json AdvTile::exportJSON()
{
  nlohmann::json link;
  link["type"] = "neighbour";
  if (neighbours.n) {
    link["name"] = "north";
    link["target"] = neighbours.n->object["id"];
    object["links"].push_back(link);
  }
  if (neighbours.ne) {
    link["name"] = "north_east";
    link["target"] = neighbours.ne->object["id"];
    object["links"].push_back(link);
  }
  if (neighbours.e) {
    link["name"] = "east";
    link["target"] = neighbours.e->object["id"];
    object["links"].push_back(link);
  }
  if (neighbours.se) {
    link["name"] = "south_east";
    link["target"] = neighbours.se->object["id"];
    object["links"].push_back(link);
  }
  if (neighbours.s) {
    link["name"] = "south";
    link["target"] = neighbours.s->object["id"];
    object["links"].push_back(link);
  }
  if (neighbours.sw) {
    link["name"] = "south_west";
    link["target"] = neighbours.sw->object["id"];
    object["links"].push_back(link);
  }
  if (neighbours.w) {
    link["name"] = "west";
    link["target"] = neighbours.w->object["id"];
    object["links"].push_back(link);
  }
  if (neighbours.nw) {
    link["name"] = "north_west";
    link["target"] = neighbours.nw->object["id"];
    object["links"].push_back(link);
  }
  if (!biome.empty()) object["classes"].push_back(biome);
  return object;
}
