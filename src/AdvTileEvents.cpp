/*
 * This file is part of the ADV Project
 * Text-based adventure game, highly modular and customizable
 * Written by Marcelo López Minnucci <coloconazo@gmail.com>
 * and Mariano López Minnucci <mlopezviedma@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

void AdvTile::erosion(double strength, double weight)
{
  std::vector<AdvTile*> n;
  if (neighbours.n) n.push_back(neighbours.n);
  if (neighbours.ne) n.push_back(neighbours.ne);
  if (neighbours.e) n.push_back(neighbours.e);
  if (neighbours.se) n.push_back(neighbours.se);
  if (neighbours.s) n.push_back(neighbours.s);
  if (neighbours.sw) n.push_back(neighbours.sw);
  if (neighbours.w) n.push_back(neighbours.w);
  if (neighbours.nw) n.push_back(neighbours.nw);
  if (!n.size()) return;
  AdvTile* m = nullptr;
  double delta = weight*(strength + strength*((double) rand()/(double) RAND_MAX));
  if (((nerosions+10)) >= (rand() % 100)) {
    m = n.at(rand() % n.size());
    altitude += (delta);
    ++nerosions;
    m->altitude -= (delta);
    m->nerosions += 5;
  }
  return;
}

void AdvTile::collapse(double strength, double weight)
{
  std::vector<AdvTile*> n;
  if (neighbours.n) n.push_back(neighbours.n);
  if (neighbours.ne) n.push_back(neighbours.ne);
  if (neighbours.e) n.push_back(neighbours.e);
  if (neighbours.se) n.push_back(neighbours.se);
  if (neighbours.s) n.push_back(neighbours.s);
  if (neighbours.sw) n.push_back(neighbours.sw);
  if (neighbours.w) n.push_back(neighbours.w);
  if (neighbours.nw) n.push_back(neighbours.nw);
  double gradient = 0;
  AdvTile* m = nullptr;
  for (auto& x: n) if (abs(altitude - x->altitude) > gradient) {
    gradient = abs(altitude - x->altitude);
    m = x;
  }
  if (gradient < (50.0 + 100.0*((double) rand()/(double) RAND_MAX))/2) return;
  double delta = weight*gradient*(strength*((double) rand()/(double) RAND_MAX)/200.0);
  if ( (altitude - m->altitude) > 0) {
    altitude -= delta;
    m->altitude += delta;
  }
  else {
    altitude += delta;
    m->altitude -= delta;
  }
  return;
}

void AdvTile::updateTemperature(double world_min, double world_max, double world_max_gradient)
{
  std::vector<AdvTile*> n;
  if (neighbours.n) n.push_back(neighbours.n);
  if (neighbours.ne) n.push_back(neighbours.ne);
  if (neighbours.e) n.push_back(neighbours.e);
  if (neighbours.se) n.push_back(neighbours.se);
  if (neighbours.s) n.push_back(neighbours.s);
  if (neighbours.sw) n.push_back(neighbours.sw);
  if (neighbours.w) n.push_back(neighbours.w);
  if (neighbours.nw) n.push_back(neighbours.nw);
  if (!n.size()) return;
  double min = n.at(0)->temperature;
  double max = n.at(0)->temperature;
  for (auto& x: n) {
    if (x->temperature < min) min = x->temperature;
    if (x->temperature > max) max = x->temperature;
  }
  double min_temperature = max - world_max_gradient;
  if (min_temperature < world_min) min_temperature = world_min;
  double max_temperature = min + world_max_gradient;
  if (max_temperature > world_max) max_temperature = world_max;
  temperature = min_temperature + ((double) rand()/(double) RAND_MAX)*(max_temperature - min_temperature);
}

void AdvTile::updatePressure(double world_min, double world_max, double world_max_gradient)
{
  std::vector<AdvTile*> n;
  if (neighbours.n) n.push_back(neighbours.n);
  if (neighbours.ne) n.push_back(neighbours.ne);
  if (neighbours.e) n.push_back(neighbours.e);
  if (neighbours.se) n.push_back(neighbours.se);
  if (neighbours.s) n.push_back(neighbours.s);
  if (neighbours.sw) n.push_back(neighbours.sw);
  if (neighbours.w) n.push_back(neighbours.w);
  if (neighbours.nw) n.push_back(neighbours.nw);
  if (!n.size()) return;
  double min = n.at(0)->pressure;
  double max = n.at(0)->pressure;
  for (auto& x: n) {
    if (x->pressure < min) min = x->pressure;
    if (x->pressure > max) max = x->pressure;
  }
  double min_pressure = max - world_max_gradient;
  if (min_pressure < world_min) min_pressure = world_min;
  double max_pressure = min + world_max_gradient;
  if (max_pressure > world_max) max_pressure = world_max;
  pressure = min_pressure + ((double) rand()/(double) RAND_MAX)*(max_pressure - min_pressure);
}
