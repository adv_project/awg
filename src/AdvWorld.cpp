/*
 * This file is part of the ADV Project
 * Text-based adventure game, highly modular and customizable
 * Written by Marcelo López Minnucci <coloconazo@gmail.com>
 * and Mariano López Minnucci <mlopezviedma@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "../include/AdvWorld.h"
#include "AdvWorldEvents.cpp"

AdvWorld::AdvWorld()
{
  world_file_path = "awg.json";
  world_class_name = "world";
  object["chrons"] = 0;
  object["meta_path"] = "meta.json";
  object["ignore_mods"] = nlohmann::json::parse("[]");
  object["only_mods"] = nlohmann::json::parse("[]");
  object["recipe"] = nlohmann::json::parse("[]");
}

bool AdvWorld::loadWorldFile()
{
  log(std::string("Loading world from ") + object["world_source"].get<std::string>() +
    " and " + object["meta_source"].get<std::string>());
  std::ifstream file;
  nlohmann::json world;
  file.open(object["world_source"].get<std::string>());
  if (file.good()) {
    try {
      file >> world;
    } catch (...) {
      log(std::string("Couldn't read JSON from file ") + object["world_source"].get<std::string>());
      file.close();
      return false;
    }
    file.close();
  }
  else {
    log(std::string("Couldn't read world file ") + object["world_source"].get<std::string>());
    return false;
  }
  file.open(object["meta_source"].get<std::string>());
  if (file.good()) {
    try {
      file >> meta;
    } catch (...) {
      log(std::string("Couldn't read JSON from file ") + object["meta_source"].get<std::string>());
      file.close();
      return false;
    }
    file.close();
  }
  else {
    log(std::string("Couldn't read meta file ") + object["meta_source"].get<std::string>());
    return false;
  }
  try {
    meta["new_input"] = meta["input"];
    if (AdvTile::hasParameter(&object, "chrons")) meta["new_input"]["chrons"] = object["chrons"]; else meta["new_input"]["chrons"] = 0;
    meta["new_input"].erase("world_source");
    meta["new_input"].erase("meta_source");
    if (AdvTile::hasParameter(&object, "world_path")) meta["new_input"]["world_path"] = object["world_path"]; else meta["new_input"]["world_path"] = "awg.json";
    if (AdvTile::hasParameter(&object, "meta_path")) meta["new_input"]["meta_path"] = object["meta_path"]; else meta["new_input"]["meta_path"] = "meta.json";
    if (AdvTile::hasParameter(&object, "log_path")) meta["new_input"]["log_path"] = object["log_path"]; else meta["new_input"].erase("log_path");
    if (AdvTile::hasParameter(&object, "ignore_mods")) meta["new_input"]["ignore_mods"] = object["ignore_mods"]; else meta["new_input"]["ignore_mods"] = nlohmann::json::parse("[]");
    if (AdvTile::hasParameter(&object, "only_mods")) meta["new_input"]["only_mods"] = object["only_mods"]; else meta["new_input"]["only_mods"] = nlohmann::json::parse("[]");
    if (AdvTile::hasParameter(&object, "recipe")) meta["new_input"]["recipe"] = object["recipe"]; else meta["new_input"]["recipe"] = nlohmann::json::parse("[]");
    object = meta["new_input"];
    meta.erase("new_input");
  } catch (...) {
    log("Bad input object on meta file");
    return false;
  }
  log(std::string("Loading tiles from world file"));
  unsigned long t = 0;
  for (unsigned long i = 0; i < world.size(); ++i) {
    try {
      if (world[i]["id"].get<std::string>() == "world") { // World object
        if (AdvTile::hasParameter(&world[i]["attributes"], "latitude_delta")) object["attributes"]["latitude_delta"] = world[i]["attributes"]["latitude_delta"];
      }
      else if (AdvTile::hasParameter(&world[i]["attributes"], "area") && AdvTile::hasParameter(&world[i]["attributes"], "perimeter")) { // Ecoregion object
      }
      else { // Tile object
        AdvTile tile;
        ids_index[world[i]["id"].get<std::string>()] = t++;
        if (AdvTile::hasParameter(&world[i]["attributes"], "altitude")) tile.altitude = world[i]["attributes"]["altitude"];
        if (AdvTile::hasParameter(&world[i]["attributes"], "atm_humidity")) tile.atm_humidity = world[i]["attributes"]["atm_humidity"];
        if (AdvTile::hasParameter(&world[i]["attributes"], "humidity_absorption")) tile.humidity_absorption = world[i]["attributes"]["humidity_absorption"];
        if (AdvTile::hasParameter(&world[i]["attributes"], "is_altitude_accident")) tile.is_altitude_accident = world[i]["attributes"]["is_altitude_accident"];
        if (AdvTile::hasParameter(&world[i]["attributes"], "is_waterfall")) tile.is_waterfall = world[i]["attributes"]["is_waterfall"];
        if (AdvTile::hasParameter(&world[i]["attributes"], "latitude")) tile.latitude = world[i]["attributes"]["latitude"];
        if (AdvTile::hasParameter(&world[i]["attributes"], "longitude")) tile.longitude = world[i]["attributes"]["longitude"];
        if (AdvTile::hasParameter(&world[i]["attributes"], "nerosions")) tile.nerosions = world[i]["attributes"]["nerosions"];
        if (AdvTile::hasParameter(&world[i]["attributes"], "nwaterfalls")) tile.nwaterfalls = world[i]["attributes"]["nwaterfalls"];
        if (AdvTile::hasParameter(&world[i]["attributes"], "pressure")) tile.pressure = world[i]["attributes"]["pressure"];
        if (AdvTile::hasParameter(&world[i]["attributes"], "salinity")) tile.salinity = world[i]["attributes"]["salinity"];
        if (AdvTile::hasParameter(&world[i]["attributes"], "soil_humidity")) tile.soil_humidity = world[i]["attributes"]["soil_humidity"];
        if (AdvTile::hasParameter(&world[i]["attributes"], "temperature")) tile.temperature = world[i]["attributes"]["temperature"];
        if (world[i]["classes"].size()) tile.biome = world[i]["classes"][0];
        tile.object["links"] = world[i]["links"];
        tile.object["id"] = world[i]["id"];
        tiles.push_back(tile);
      }
    } catch (...) {
      log(std::string("Bad object on world file at index ") + std::to_string(i));
      return false;
    }
  }
  log(std::string("Loading tile links"));
  for (int i = 0; i < tiles.size(); ++i) {
    try {
      for (int l = 0; l < tiles.at(i).object["links"].size(); ++l) {
        if (tiles.at(i).object["links"][l]["type"] == "neighbour") {
          if (tiles.at(i).object["links"][l]["name"] == "north")
            tiles.at(i).neighbours.n = &tiles.at(ids_index[tiles.at(i).object["links"][l]["target"].get<std::string>()]);
          else if (tiles.at(i).object["links"][l]["name"] == "north_east")
            tiles.at(i).neighbours.ne = &tiles.at(ids_index[tiles.at(i).object["links"][l]["target"].get<std::string>()]);
          else if (tiles.at(i).object["links"][l]["name"] == "east")
            tiles.at(i).neighbours.e = &tiles.at(ids_index[tiles.at(i).object["links"][l]["target"].get<std::string>()]);
          else if (tiles.at(i).object["links"][l]["name"] == "south_east")
            tiles.at(i).neighbours.se = &tiles.at(ids_index[tiles.at(i).object["links"][l]["target"].get<std::string>()]);
          else if (tiles.at(i).object["links"][l]["name"] == "south")
            tiles.at(i).neighbours.s = &tiles.at(ids_index[tiles.at(i).object["links"][l]["target"].get<std::string>()]);
          else if (tiles.at(i).object["links"][l]["name"] == "south_west")
            tiles.at(i).neighbours.sw = &tiles.at(ids_index[tiles.at(i).object["links"][l]["target"].get<std::string>()]);
          else if (tiles.at(i).object["links"][l]["name"] == "west")
            tiles.at(i).neighbours.w = &tiles.at(ids_index[tiles.at(i).object["links"][l]["target"].get<std::string>()]);
          else if (tiles.at(i).object["links"][l]["name"] == "north_west")
            tiles.at(i).neighbours.nw = &tiles.at(ids_index[tiles.at(i).object["links"][l]["target"].get<std::string>()]);
        }
      }
    } catch (...) {
      log(std::string("Bad link on world file at index ") + std::to_string(i));
      return false;
    }
  }
  refreshTiles();
  log(std::string("World loaded succesfully"));
  return true;
}

void AdvWorld::refreshTiles()
{
  log("Refreshing tile parameters");
  for (auto& tile: tiles) tile.refreshJSON();
}

// Write the JSON files for the world and every tile in it
void AdvWorld::exportWorld()
{
  log("Exporting world");
  std::ofstream file;
  nlohmann::json world = nlohmann::json::parse("[]");
  nlohmann::json world_object, ecoregion_object, link;
  world_object["classes"] = nlohmann::json::parse("[]");
  world_object["classes"].push_back(world_class_name);
  world_object["id"] = link["name"] = link["target"] = "world";
  link["type"] = "parent";
  for (auto& tile: tiles) {
    tile.object["links"] = nlohmann::json::parse("[]");
    tile.object["links"].push_back(link);
  }
  link["type"] = "ecoregion";
  if (AdvTile::hasParameter(&object, "custom_attributes"))
    world_object["attributes"] = object["custom_attributes"];
  if (AdvTile::hasParameter(&object["attributes"], "latitude_delta"))
    world_object["attributes"]["latitude_delta"] = object["attributes"]["latitude_delta"];
  world.push_back(world_object);
  for (auto& x: meta["ecoregions"].items()) {
    for (auto& y: x.value()["classes"].items()) {
      ecoregion_object.clear();
      ecoregion_object["id"] = y.key();
      ecoregion_object["classes"].push_back(x.key());
      ecoregion_object["attributes"] = y.value();
      world.push_back(ecoregion_object);
    }
    for (auto& y: x.value()["tiles"].items()) {
      if (y.value()["border"]) link["name"] = "border"; else link["name"] = "interior";
      link["target"] = y.value()["class"];
      tiles.at((unsigned long) ids_index[y.key()]).object["links"].push_back(link);
    }
  }
  for (auto& tile: tiles) world.push_back(tile.exportJSON());
  file.open(world_file_path);
  file << std::setw(2) << world << std::endl;
  file.close();
  if (AdvTile::hasParameter(&object, "meta_path") && object["meta_path"].type() == nlohmann::json::value_t::string) {
    meta["input"] = object;
    file.open(object["meta_path"].get<std::string>());
    file << std::setw(2) << meta << std::endl;
    file.close();
  }
  log(std::string("A new world is ready at ") + world_file_path);
}

// Generate a new world
bool AdvWorld::generateWorld(nlohmann::json world)
{
  try { world_file_path = world["world_path"]; } catch (...) {}
  try { world_class_name = world["world_class"]; } catch (...) {}
  try { object["meta_path"] = world["meta_path"]; } catch (...) {}
  try { object["log_path"] = world["log_path"]; } catch (...) {}
  time_t now = time(0);
  tm *t = localtime(&now);
  char date[32];
  strftime(&date[0], 32, "%F %H:%M:%S", t);
  log(std::string("[") + date + "] Starting ADV World Generator", true);
  nlohmann::json mod_world_json;
  std::ifstream mod_world_file;
  bool ret = true;
  mod_world_file.open("mods/awg/assets/world.json");
  if (mod_world_file.good()) {
    try {
      mod_world_file >> mod_world_json;
      object["attributes"] = mod_world_json; // Load default values
    } catch (...) {}
    mod_world_file.close();
  }
  loadWorldObject(world);
  if ( AdvTile::hasParameter(&object, "world_source") && object["world_source"].type() == nlohmann::json::value_t::string &&
    AdvTile::hasParameter(&object, "meta_source") && object["meta_source"].type() == nlohmann::json::value_t::string ) {
    ret = loadWorldFile();
  }
  else if (object["attributes"]["geometry"]["type"].get<std::string>() == "planet") {
    generatePlanet(object["attributes"]["geometry"]["size"]);
    object["attributes"]["latitude_delta"] = 180.0/(double) object["attributes"]["geometry"]["size"];
    for (unsigned long x = 0; x < 2*(unsigned long) object["attributes"]["geometry"]["size"]; ++x)
      for (unsigned long y = 0; y < object["attributes"]["geometry"]["size"]; ++y) {
        tiles.at(2*(unsigned long) object["attributes"]["geometry"]["size"]*y+x).latitude =
          90.0*(1.0 - (double) (1 + 2*y)/(double) object["attributes"]["geometry"]["size"]);
        tiles.at(2*(unsigned long) object["attributes"]["geometry"]["size"]*y+x).longitude =
          180.0*((double) (1 + 2*x)/2.0/(double) object["attributes"]["geometry"]["size"] - 1.0);
      }
  }
  else if (object["attributes"]["geometry"]["type"].get<std::string>() == "grid") {
    generateGrid(object["attributes"]["geometry"]["width"], object["attributes"]["geometry"]["height"]);
    double h = object["attributes"]["geometry"]["height"];
    double w = object["attributes"]["geometry"]["width"];
    double delta, x0, y0;
    if (AdvTile::hasParameter(&object["attributes"]["geometry"], "central_latitude") &&
      (abs((double) object["attributes"]["geometry"]["central_latitude"]) < 90.0) ) {
      delta = (90 - abs((double) object["attributes"]["geometry"]["central_latitude"]))/(h+1)/2;
      if (delta > 1) delta = 1;
      delta *= (0.1 + 0.8*((double) rand()/(double) RAND_MAX));
      y0 = (double) object["attributes"]["geometry"]["central_latitude"] + delta*h/2;
    }
    else {
      double dh = 180.0/(double) object["attributes"]["geometry"]["height"];
      double dw = 360.0/(double) object["attributes"]["geometry"]["width"];
      (dw < dh) ? delta = dw : delta = dh;
      if (delta > 1) delta = 1;
      delta *= (0.1 + 0.8*((double) rand()/(double) RAND_MAX));
      y0 = 90 - (180.0 + delta + delta*(h-1))*((double) rand()/(double) RAND_MAX);
    }
    object["attributes"]["latitude_delta"] = delta;
    x0 = -180 + (360.0 - delta - delta*(w-1))*((double) rand()/(double) RAND_MAX);
    for (unsigned long x = 0; x < object["attributes"]["geometry"]["width"]; ++x)
      for (unsigned long y = 0; y < object["attributes"]["geometry"]["height"]; ++y) {
        tiles.at((unsigned long) object["attributes"]["geometry"]["width"]*y+x).latitude =
          y0 - delta/2 - delta*y;
        tiles.at((unsigned long) object["attributes"]["geometry"]["width"]*y+x).longitude =
          x0 + delta/2 + delta*x;
      }
  }
  if (ret) log("Generating random selection tiles's index");
  if (ret) for (unsigned long i = 0; i < tiles.size(); ++i) index.push_back(i);
  return ret;
}

void AdvWorld::generatePlanet(unsigned long size)
{
  unsigned long width = 2*size;
  unsigned long height = size;
  generateGrid(width, height);
  log(std::string("Generating ") + std::to_string(size) + "-sized planet");
  for (unsigned long y = 1; y < height-1; ++y) {
    tiles.at(width*y).neighbours.w = &tiles.at(width*y+width-1);
    tiles.at(width*y).neighbours.nw = &tiles.at(width*(y-1)+width-1);
    tiles.at(width*y).neighbours.sw = &tiles.at(width*(y+1)+width-1);
    tiles.at(width*y+width-1).neighbours.e = &tiles.at(width*y);
    tiles.at(width*y+width-1).neighbours.ne = &tiles.at(width*(y-1));
    tiles.at(width*y+width-1).neighbours.se = &tiles.at(width*(y+1));
  }
  tiles.at(0).neighbours.w = &tiles.at(width-1);
  tiles.at(0).neighbours.sw = &tiles.at(2*width-1);
  tiles.at(width-1).neighbours.e = &tiles.at(0);
  tiles.at(width-1).neighbours.se = &tiles.at(width);
  tiles.at(width*(height-1)).neighbours.w = &tiles.at(height*width-1);
  tiles.at(width*(height-1)).neighbours.nw = &tiles.at((height-1)*width-1);
  tiles.at(height*width-1).neighbours.e = &tiles.at(width*(height-1));
  tiles.at(height*width-1).neighbours.ne = &tiles.at(width*(height-2));
  for (unsigned long x = 0; x < width/2; ++x) {
    tiles.at(x).neighbours.n = &tiles.at(x+width/2);
    tiles.at(x+width/2).neighbours.n = &tiles.at(x);
    tiles.at(width*(height-1)+x).neighbours.s = &tiles.at(width*(height-1)+x+width/2);
    tiles.at(width*(height-1)+x+width/2).neighbours.s = &tiles.at(width*(height-1)+x);
  }
  for (unsigned long x = 0; x < width; ++x) {
    tiles.at(x).neighbours.nw = tiles.at(x).neighbours.n->neighbours.w;
    tiles.at(x).neighbours.ne = tiles.at(x).neighbours.n->neighbours.e;
    tiles.at(width*(height-1)+x).neighbours.sw = tiles.at(width*(height-1)+x).neighbours.s->neighbours.w;
    tiles.at(width*(height-1)+x).neighbours.se = tiles.at(width*(height-1)+x).neighbours.s->neighbours.e;
  }
}

void AdvWorld::generateGrid(unsigned long width, unsigned long height)
{
  log(std::string("Building ") + std::to_string(width) + "x" + std::to_string(height) + " grid");
  AdvTile tile;
  nlohmann::json row;
  unsigned long ntile = 1;
  tile.altitude = 0;
  tile.salinity = ((double) object["attributes"]["salinity"]["maximum"] - (double) object["attributes"]["salinity"]["minimum"]) / 2.0;
  tile.atm_humidity = (double) object["attributes"]["atm_humidity"]["default"];
  tile.soil_humidity = ((double) object["attributes"]["soil_humidity"]["maximum"] - (double) object["attributes"]["soil_humidity"]["minimum"]) / 2.0;
  tile.humidity_absorption = object["attributes"]["humidity_absorption"]["default"];
  tile.temperature = ((double) object["attributes"]["temperature"]["maximum"] - (double) object["attributes"]["temperature"]["minimum"]) / 2.0;
  tile.pressure = object["attributes"]["pressure"]["default"];
  tiles.resize(width*height, tile);
  meta["tiles_matrix"] = nlohmann::json::parse("[]");
  for (unsigned long x = 0; x < width; ++x) {
    row = nlohmann::json::parse("[]");
    for (unsigned long y = 0; y < height; ++y) {
      tiles.at(width*y+x).object["id"] = std::string("tile") + std::to_string(ntile++);
      ids_index[tiles.at(width*y+x).object["id"].get<std::string>()] = (unsigned long) width*y+x;
      row.push_back(tiles.at(width*y+x).object["id"]);
      if (x > 0) {
        tiles.at(width*y+x).neighbours.w = &tiles.at(width*y+(x-1));
        if (y > 0) tiles.at(width*y+x).neighbours.nw = &tiles.at(width*(y-1)+(x-1));
        if (y < height-1) tiles.at(width*y+x).neighbours.sw = &tiles.at(width*(y+1)+(x-1));
      }
      if (x < width-1) {
        tiles.at(width*y+x).neighbours.e = &tiles.at(width*y+(x+1));
        if (y > 0) tiles.at(width*y+x).neighbours.ne = &tiles.at(width*(y-1)+(x+1));
        if (y < height-1) tiles.at(width*y+x).neighbours.se = &tiles.at(width*(y+1)+(x+1));
      }
      if (y > 0) tiles.at(width*y+x).neighbours.n = &tiles.at(width*(y-1)+x);
      if (y < height-1) tiles.at(width*y+x).neighbours.s = &tiles.at(width*(y+1)+x);
    }
    meta["tiles_matrix"].push_back(row);
  }
  object["attributes"]["chrons"] = 0;
}

// Move nchrons chrons forward, drastically changing the world
void AdvWorld::advanceChron(unsigned int nchrons)
{
  log(std::string("Advancing ") + std::to_string(nchrons) + " chrons");
  int amount;
  double ep = ((double) object["attributes"]["erosion"]["percentage"])/100.0;
  double er = ((double) object["attributes"]["erosion"]["randomness"])/100.0;
  double cp = ((double) object["attributes"]["collapse"]["percentage"])/100.0;
  double cr = ((double) object["attributes"]["collapse"]["randomness"])/100.0;
  double tempp = ((double) object["attributes"]["temperature"]["percentage"])/100.0;
  double tempr = ((double) object["attributes"]["temperature"]["randomness"])/100.0;
  double min_temperature = (double) object["attributes"]["temperature"]["minimum"];
  double max_temperature = (double) object["attributes"]["temperature"]["maximum"];
  double max_temperature_gradient = (double) object["attributes"]["temperature"]["maximum_gradient"];
  double pp = ((double) object["attributes"]["pressure"]["percentage"])/100.0;
  double pr = ((double) object["attributes"]["pressure"]["randomness"])/100.0;
  double min_pressure = (double) object["attributes"]["pressure"]["minimum"];
  double max_pressure = (double) object["attributes"]["pressure"]["maximum"];
  double max_pressure_gradient = (double) object["attributes"]["pressure"]["maximum_gradient"];
  for (int chron = 0; chron < nchrons; ++chron) {
    std::random_shuffle(index.begin(), index.end());
    amount = (ep-er + 2.0*er*((double) rand()/(double) RAND_MAX))*tiles.size();
    if (amount > tiles.size()) amount = tiles.size();
    for (int i = 0; i < amount; ++i)
      tiles.at(index.at(i)).erosion((double) object["attributes"]["erosion"]["strength"]);
    std::random_shuffle(index.begin(), index.end());
    amount = (cp-cr + 2.0*cr*((double) rand()/(double) RAND_MAX))*tiles.size();
    if (amount > tiles.size()) amount = tiles.size();
    for (int i = 0; i < amount; ++i)
      tiles.at(index.at(i)).collapse((double) object["attributes"]["collapse"]["strength"]);
    std::random_shuffle(index.begin(), index.end());
    amount = (tempp-tempr + 2.0*tempr*((double) rand()/(double) RAND_MAX))*tiles.size();
    if (amount > tiles.size()) amount = tiles.size();
    for (int i = 0; i < amount; ++i) tiles.at(index.at(i)).updateTemperature(min_temperature, max_temperature,
      max_temperature_gradient);
  }
  adjustSeaLevel();
  compressAltitude();
  adjustSoilHumidity();
  adjustPressure();
  for (int chron = 0; chron < nchrons; ++chron) {
    std::random_shuffle(index.begin(), index.end());
    amount = (pp-pr + 2.0*pr*((double) rand()/(double) RAND_MAX))*tiles.size();
    if (amount > tiles.size()) amount = tiles.size();
    for (int i = 0; i < amount; ++i) tiles.at(index.at(i)).updatePressure(min_pressure, max_pressure,
      max_pressure_gradient);
  }
  object["attributes"]["chrons"] = (unsigned long) object["attributes"]["chrons"] + nchrons;
}

void AdvWorld::log(std::string text, bool start)
{
  if (AdvTile::hasParameter(&object, "log_path") && object["log_path"].type() == nlohmann::json::value_t::string) {
    std::chrono::duration<double> duration = std::chrono::steady_clock::duration::zero();
    if (start)
      log_reference_time = std::chrono::high_resolution_clock::now();
    else
      duration = std::chrono::duration_cast<std::chrono::duration<double>>
        (std::chrono::high_resolution_clock::now() - log_reference_time);
    std::ofstream file(object["log_path"].get<std::string>(), std::ios::out | std::ios::app);
    file << "[" << duration.count() << "] " << text << std::endl;
    file.close();
  }
}

std::vector<AdvTile*> AdvWorld::generatePath(unsigned int nsteps)
{
  std::vector<AdvTile*> ret;
  AdvTile *tile = &tiles.at(rand() % tiles.size());
  short direction = rand() % 8;
  ret.push_back(tile);
  for (unsigned int step = 0; step < nsteps; ++step) {
    for (int i = 0; i < 3; ++i) {
      switch (direction) {
        case 0: tile = tile->neighbours.n; break;
        case 1: tile = tile->neighbours.ne; break;
        case 2: tile = tile->neighbours.e; break;
        case 3: tile = tile->neighbours.se; break;
        case 4: tile = tile->neighbours.s; break;
        case 5: tile = tile->neighbours.sw; break;
        case 6: tile = tile->neighbours.w; break;
        case 7: tile = tile->neighbours.nw; break;
      }
      if (!tile) break;
      ret.push_back(tile);
    }
    if (!tile) break;
    switch (direction) {
      case 0: direction = (7 + rand() % 3) % 8; break;
      case 1: direction = (rand() % 3) % 8; break;
      case 2: direction = (1 + rand() % 3) % 8; break;
      case 3: direction = (2 + rand() % 3) % 8; break;
      case 4: direction = (3 + rand() % 3) % 8; break;
      case 5: direction = (4 + rand() % 3) % 8; break;
      case 6: direction = (5 + rand() % 3) % 8; break;
      case 7: direction = (6 + rand() % 3) % 8; break;
    }
  }
  return ret;
}

double AdvWorld::getLandPercentage(double sea_level)
{
  if (!tiles.size()) return 0;
  unsigned long ret = 0;
  for (auto& tile: tiles) {
    if (tile.altitude > sea_level) ++ret;
  }
  return 100.0*( (double) ret / (double) tiles.size() );
}

// Restrict param between 0 and 100
void AdvWorld::fixPercentage(nlohmann::json &param)
{
  try {
    if (param < 0) param = 0.0;
    if (param > 100) param = 100.0;
  } catch (...) { param = 0.0; }
}

void AdvWorld::loadWorldObject(nlohmann::json world)
{
  log("Loading input parameters");
  try { object["chrons"] = (unsigned long) world["chrons"]; } catch (...) {}
  try { object["world_source"] = world["world_source"].get<std::string>(); } catch (...) {}
  try { object["meta_source"] = world["meta_source"].get<std::string>(); } catch (...) {}
  try {
    if (world["geometry"]["type"].get<std::string>() == "planet") {
      object["attributes"]["geometry"]["type"] = "planet";
      object["attributes"]["geometry"]["size"] = (unsigned long) world["geometry"]["size"];
    }
    else if (world["geometry"]["type"].get<std::string>() == "grid") {
      object["attributes"]["geometry"]["type"] = "grid";
      if (AdvTile::hasParameter(&world["geometry"], "size")) {
        object["attributes"]["geometry"]["width"] = (unsigned long) world["geometry"]["size"];
        object["attributes"]["geometry"]["height"] = (unsigned long) world["geometry"]["size"];
      }
      else {
        object["attributes"]["geometry"]["height"] = (unsigned long) world["geometry"]["height"];
        object["attributes"]["geometry"]["width"] = (unsigned long) world["geometry"]["width"];
      }
      object["attributes"]["geometry"].erase("size");
    }
  } catch (...) {}
  try { object["attributes"]["altitude"]["minimum"] = (double) world["altitude"]["minimum"]; } catch (...) {}
  try { object["attributes"]["altitude"]["maximum"] = (double) world["altitude"]["maximum"]; } catch (...) {}
  try { object["attributes"]["salinity"]["minimum"] = (double) world["salinity"]["minimum"]; } catch (...) {}
  try { object["attributes"]["salinity"]["maximum"] = (double) world["salinity"]["maximum"]; } catch (...) {}
  try { object["attributes"]["atm_humidity"]["minimum"] = (double) world["atm_humidity"]["minimum"]; } catch (...) {}
  try { object["attributes"]["atm_humidity"]["maximum"] = (double) world["atm_humidity"]["maximum"]; } catch (...) {}
  try { object["attributes"]["soil_humidity"]["minimum"] = (double) world["soil_humidity"]["minimum"]; } catch (...) {}
  try { object["attributes"]["soil_humidity"]["maximum"] = (double) world["soil_humidity"]["maximum"]; } catch (...) {}
  try { object["attributes"]["humidity_absorption"]["default"] = (double) world["humidity_absorption"]["default"]; } catch (...) {}
  try { object["attributes"]["temperature"]["minimum"] = (double) world["temperature"]["minimum"]; } catch (...) {}
  try { object["attributes"]["temperature"]["maximum"] = (double) world["temperature"]["maximum"]; } catch (...) {}
  try { object["attributes"]["temperature"]["maximum_gradient"] = (double) world["temperature"]["maximum_gradient"]; } catch (...) {}
  try { object["attributes"]["pressure"]["default"] = (double) world["pressure"]["default"]; } catch (...) {}
  try { object["attributes"]["pressure"]["minimum"] = (double) world["pressure"]["minimum"]; } catch (...) {}
  try { object["attributes"]["pressure"]["maximum"] = (double) world["pressure"]["maximum"]; } catch (...) {}
  try { object["attributes"]["pressure"]["maximum_gradient"] = (double) world["pressure"]["maximum_gradient"]; } catch (...) {}
  try { object["attributes"]["land_percentage"] = (double) world["land_percentage"]; } catch (...) {}
  try { object["attributes"]["world_tilt"] = (double) world["world_tilt"]; } catch (...) {}
  try { object["attributes"]["accidents"]["strength"] = (double) world["accidents"]["strength"]; } catch (...) {}
  try { object["attributes"]["erosion"]["percentage"] = (double) world["erosion"]["percentage"]; } catch (...) {}
  try { object["attributes"]["erosion"]["randomness"] = (double) world["erosion"]["randomness"]; } catch (...) {}
  try { object["attributes"]["erosion"]["strength"] = (double) world["erosion"]["strength"]; } catch (...) {}
  try { object["attributes"]["collapse"]["percentage"] = (double) world["collapse"]["percentage"]; } catch (...) {}
  try { object["attributes"]["collapse"]["randomness"] = (double) world["collapse"]["randomness"]; } catch (...) {}
  try { object["attributes"]["collapse"]["strength"] = (double) world["collapse"]["strength"]; } catch (...) {}
  try { object["attributes"]["waterfall"]["percentage"] = (double) world["waterfall"]["percentage"]; } catch (...) {}
  try { object["attributes"]["waterfall"]["randomness"] = (double) world["waterfall"]["randomness"]; } catch (...) {}
  if ((double) object["attributes"]["altitude"]["minimum"] >= 0) object["attributes"]["altitude"]["minimum"] = 0.0;
  if ((double) object["attributes"]["altitude"]["maximum"] <= 0) object["attributes"]["altitude"]["maximum"] = 0.0;
  fixPercentage(object["attributes"]["salinity"]["minimum"]);
  fixPercentage(object["attributes"]["salinity"]["maximum"]);
  fixPercentage(object["attributes"]["atm_humidity"]["default"]);
  fixPercentage(object["attributes"]["soil_humidity"]["maximum"]);
  fixPercentage(object["attributes"]["humidity_absorption"]["default"]);
  fixPercentage(object["attributes"]["land_percentage"]);
  fixPercentage(object["attributes"]["erosion"]["percentage"]);
  fixPercentage(object["attributes"]["erosion"]["randomness"]);
  if ((double) object["attributes"]["accidents"]["strength"] <= 0) object["attributes"]["accidents"]["strength"] = 0.0;
  if ((double) object["attributes"]["erosion"]["strength"] <= 0) object["attributes"]["erosion"]["strength"] = 0.0;
  fixPercentage(object["attributes"]["collapse"]["strength"]);
  fixPercentage(object["attributes"]["collapse"]["percentage"]);
  fixPercentage(object["attributes"]["collapse"]["randomness"]);
  try { object["attributes"]["geometry"]["central_latitude"] = (double) world["geometry"]["central_latitude"]; } catch (...) {}
  if (AdvTile::hasParameter(&world, "attributes") && world["attributes"].type() == nlohmann::json::value_t::object)
    object["custom_attributes"] = world["attributes"];
  if (AdvTile::hasParameter(&world, "ignore_mods") && world["ignore_mods"].type() == nlohmann::json::value_t::array &&
    world["ignore_mods"].size() > 0 && world["ignore_mods"][0].type() == nlohmann::json::value_t::string)
    object["ignore_mods"] = world["ignore_mods"];
  if (AdvTile::hasParameter(&world, "only_mods") && world["only_mods"].type() == nlohmann::json::value_t::array &&
    world["only_mods"].size() > 0 && world["only_mods"][0].type() == nlohmann::json::value_t::string)
    object["only_mods"] = world["only_mods"];
  if (AdvTile::hasParameter(&world, "recipe") && world["recipe"].type() == nlohmann::json::value_t::array &&
    world["recipe"].size() > 0 && world["recipe"][0].type() == nlohmann::json::value_t::string)
    object["recipe"] = world["recipe"];
  if (object["recipe"].empty()) { // Default recipe
    object["recipe"].push_back("generateAltitudeAccidentPaths");
    object["recipe"].push_back("softenAltitudes");
    object["recipe"].push_back("initializeTemperature");
    object["recipe"].push_back("advanceChrons");
    object["recipe"].push_back("generateWaterfallPaths");
    object["recipe"].push_back("adjustSalinity");
    object["recipe"].push_back("generateBiomes");
    object["recipe"].push_back("generateCrusts");
    object["recipe"].push_back("generateEcoregions");
  }
}
